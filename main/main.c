/* BSD Socket API Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <string.h>
#include <sys/param.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "protocol_examples_common.h"
#include "esp_sntp.h"

#define HOST_IP_ADDR CONFIG_ZMQ_EXAMPLE_IPV4_ADDR
#define PORT CONFIG_ZMQ_EXAMPLE_PORT
#define DEVICE_ID CONFIG_ZMQ_EXAMPLE_DEVICE_ID

#include <libzmq.h>
// tests
#include <zmtp.h>
#include "zmtp_classes.h"
#include <cJSON.h>

static const char *zmq_addr = HOST_IP_ADDR;
unsigned short port = PORT;

static const char *TAG = "zmq_client";

static void initialize_sntp(void);

static void zmq_client_task(void *pvParameters)
{
    int ret;
    uint32_t rand;
    time_t now;
    struct tm timeinfo;
    time(&now);
    localtime_r(&now, &timeinfo);
    //printf("now is: %ld\n", now);
    /*
     * create JSON data structure
     */
    cJSON *root = NULL;
    cJSON *tmst = NULL;
    cJSON *fields = NULL;
    cJSON *v1 = NULL;
    cJSON *v2 = NULL;
    cJSON *tags = NULL;
    cJSON *test = NULL;

    root = cJSON_CreateObject();

    tmst = cJSON_CreateNumber(now);
    cJSON_AddItemToObject(root, "tmst", tmst);

    fields = cJSON_CreateObject();
    v1 = cJSON_CreateNumber(0);
    cJSON_AddItemToObject(fields, "v1", v1);
    v2 = cJSON_CreateNumber(0);
    cJSON_AddItemToObject(fields, "v2", v2);

    tags = cJSON_CreateObject();
    test = cJSON_CreateBool(false);
    cJSON_AddItemToObject(tags, "test", test);

    cJSON_AddItemToObject(root, "fields", fields);
    cJSON_AddItemToObject(root, "tags", tags);

    char *json_str = cJSON_PrintUnformatted(root);
    //printf("%s\n", json_str);
    char msg_buf[512];

    /*
     * ZMQ tests
     */
    //zmtp_msg_test(true);
    //printf(" * ZMQ msg test succesful!\n");
    //zmtp_channel_test(true);
    //printf(" * ZMQ channel test succesful!\n");

    /*
     * connect to ZMQ server
     */
    zmtp_dealer_t *dealer;

    dealer = zmtp_dealer_new();
    ret = zmtp_dealer_tcp_connect (dealer, zmq_addr, port);
    assert (ret == 0);

    /*
     * main loop
     */
    while(true)
    {
        rand = esp_random();
        cJSON_GetObjectItem(cJSON_GetObjectItem(root, "fields"), "v1")->valuedouble = rand/3.3;
        rand = esp_random();
        cJSON_GetObjectItem(cJSON_GetObjectItem(root, "fields"), "v2")->valuedouble = rand/2.2;

        time(&now);
        cJSON_GetObjectItem(root, "tmst")->valuedouble = now;
        json_str = cJSON_PrintUnformatted(root);
        strcpy(msg_buf, DEVICE_ID);
        strcat(msg_buf, " ");
        strcat(msg_buf, json_str);

        printf("sending ZMQ message: %s\n", msg_buf);
        zmtp_msg_t *msg = zmtp_msg_from_const_data (
            0, &msg_buf, strlen (msg_buf));
        assert (msg);
        zmtp_dealer_send(dealer, msg);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }

    // this should never be reached
    vTaskDelete(NULL);
}

static void initialize_sntp(void)
{
    ESP_LOGI(TAG, "Initializing SNTP");
    sntp_setoperatingmode(SNTP_OPMODE_POLL);
    sntp_setservername(0, "pool.ntp.org");
    //sntp_set_time_sync_notification_cb(time_sync_notification_cb);
    sntp_init();
}

void app_main()
{
    ESP_ERROR_CHECK(nvs_flash_init());
    tcpip_adapter_init();
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    /* This helper function configures Wi-Fi or Ethernet, as selected in menuconfig.
     * Read "Establishing Wi-Fi or Ethernet Connection" section in
     * examples/protocols/README.md for more information about this function.
     */
    ESP_ERROR_CHECK(example_connect());

    initialize_sntp();

    // wait for time to be set
    time_t now = 0;
    struct tm timeinfo = { 0 };
    int retry = 0;
    const int retry_count = 10;
    while (sntp_get_sync_status() == SNTP_SYNC_STATUS_RESET && ++retry < retry_count) {
        ESP_LOGI(TAG, "Waiting for system time to be set... (%d/%d)", retry, retry_count);
        vTaskDelay(2000 / portTICK_PERIOD_MS);
    }
    time(&now);
    localtime_r(&now, &timeinfo);

    xTaskCreate(zmq_client_task, "zmq_client", 4096, NULL, 5, NULL);
}
