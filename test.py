#!/usr/bin/env python3
import time
import zmq

ctx = zmq.Context()
s = ctx.socket(zmq.SUB)
s.bind('tcp://*:1234')
s.setsockopt(zmq.SUBSCRIBE, b'mo')

while True:
    msg = s.recv()
    print(msg)
